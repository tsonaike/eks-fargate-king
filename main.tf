# Fargate node groups example

#terraform {
 # required_version = "~> 1.0"
#}

#provider "aws" {
 # region = var.aws_region
#}

# vpc
module "vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  version            = "2.63.0"
  name               = var.name
  azs                = var.azs
  cidr               = "10.32.0.0/16"
  private_subnets    = ["10.32.1.0/24", "10.32.2.0/24", "10.32.3.0/24"]
  public_subnets     = ["10.32.101.0/24", "10.32.102.0/24", "10.32.103.0/24"]
  enable_nat_gateway = true
  single_nat_gateway = true
  tags               = module.eks.tags.shared
}

# eks
module "eks" {
  source              = "Young-ook/eks/aws"
  name                = var.name
  tags                = var.tags
  subnets             = module.vpc.private_subnets
  kubernetes_version  = var.kubernetes_version
  managed_node_groups = var.managed_node_groups
  node_groups         = var.node_groups
  fargate_profiles    = var.fargate_profiles
}
