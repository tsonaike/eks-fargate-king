aws_region = "us-east-1"
azs        = ["us-east-1a", "us-east-1b", "us-east-1c"]
name       = "eks-fargate"
tags = {
  env = "dev"
}
kubernetes_version = "1.22"
fargate_profiles = [
  {
    name      = "default"
    namespace = "default"
  },
]
